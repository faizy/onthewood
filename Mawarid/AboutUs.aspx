﻿<%@ Page Title="Mawarid | About Us" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="AboutUs.aspx.cs" Inherits="Mawarid.AboutUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">
    $(document).ready(function () {
        $("#lnkAboutUs").addClass("activeTop");
    });
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div class="banner">
        <img src="~/images/banner.jpg" alt="mawarid" runat="server" />
    </div>
    <div class="aboutcontent">
        
        <div class="maincont">
            <h2>
                Mawarid Finance</h2>
            <div class="clearfix">
            </div>
            <p>
                Mawarid Finance is a private joint stock company established in Dubai, UAE, towards
                the end of 2006 with a paid-up capital of one billion dirham. The company plans
                to introduce a new vision within the Islamic finance sector to both retail and corporate
                customers in the region.
                <br />
                <br />
                Mawarid Finance is the only finance company in the UAE that is completely independent.
                The company’s shares are distributed across more than 350 shareholders, with companies
                or organizations owning not more than 5% each of the capital and no individual shareholder’s
                stake exceeding 2.5%, giving the company the operational freedom to secure a leading
                position in the Islamic banking market in the UAE.
                <br />
                <br />
                Mawarid Finance specializes in the competitive development of Shariah compliant
                financial services, focusing on rapid growth through outstanding customer service.
                <br />
                <br />
                Mawarid Finance is structured as an integrated financial institution that specializes
                in providing all services related to finance and business, responsive to the rapid
                growth taking place in Dubai and modeled on the spectacular development of Dubai
                and the UAE as a whole.
                <br />
                <br />
                The Strength of Mawarid Finance derives from the exceptional talent vested in its
                personnel- dedicated and seasoned professionals with long experience and know-how
                in the modern financial services sector within different geographical services locations,
                and a deep knowledge of Islamic finance. They drive the company’s ambitious growth
                strategy in operations, sales and marketing, and communications, using their exceptional
                capabilities to accurately implement corporate plans.
                <br />
                <br />
                The company maintains a close-knit network of strategic partners and business associates,
                and the financial backing and support of well-established shareholders.
            </p>
            <h3>
                About aljazeera</h3>
            <p>
                <strong>AlJazeera Financial Services</strong> has been established as a private
                equity company in Dubai in the United Arab Emirates in 2005 with a paid up capital
                of Thirty (30) Million Emirati Dirham (AED. 30,000,000.00) specialized in the field
                of financial intermediation.<br />
                <br />
                Aljazeera acquired the Margin Trading License, and is offering such service to its
                clients.<br />
                <br />
                <strong>AlJazeera Financial Services</strong> is the Financial Intermediation Affiliate
                of MAWARID FINANCE with Financial Intermediation as its main activity through trading
                companies’ shares with activities that co-relate with the Islamic Sharia laws and
                principals.
                <br />
                <br />
                The company's activities are concentrated in the Dubai Financial Market and Abu
                Dhabi Financial market.
                <br />
                <br />
                <strong>AlJazeera Financial Services</strong> provides distinct services and employs
                qualified and trained personnel in both of its front and its back office alike putting
                the client’s satisfaction above everything else. <strong>Aljazeera Financial Services’</strong>
                clients vary from individuals to corporate and its staff and personals are well
                trained to serve their clients to the highest levels possible in accordance with
                the stated laws and regulations that are imposed by the securities and regulatory
                boards in order to enable the clients to invest their money in the most optimal
                manner.<br />
                <br />
                Since the formation of AlJazeera Financial Services meeting investor’s needs in
                providing high quality customer retention based financial mediation services and
                approach through developing new working methods and competencies in order to maintain
                its local and regional growth and goals.
            </p>
            <h3>
                Our Vision</h3>
            <p>
                To be one of the main leaders in the Financial Services Companies in GCC and ARAB
                World and lead the development and benchmarking of a unique financial service.</p>
            <h3>
                Our Mission</h3>
            <p>
                To build a partnership with our customers that lasts a lifetime by treating every
                customer as a VIP individual.</p>
            <p>
                To create superior and pioneering products and services, sustaining high-quality
                performance and maximizing market share to become the provider of first choice Islamic
                financial solutions in a modern business environment.</p>
            <h3>
                CEO Message</h3>
            <p>
                Welcome to Aljazeera’s website, designed to provide you with information on our
                history, values and profile as well as on the products and services we offer.<br />
                <br />
                Our valued clients can also have access to our advanced and highly secured trading
                platform and personal accounts. When we founded AJAZEERA, our vision was to create
                a premier and unique financial services company covering the whole of the Middle
                East and North Africa region (MENA), backed with a strong team of experienced professionals,
                advanced trading system, offering the finest services.<br />
                <br />
                We seek the trust and satisfaction of our valued clients, and provide full dedication
                of our experienced team of professionals that relentlessly provides outstanding
                client satisfaction and cutting-edge execution.<br />
                <br />
                We will continue to enhance these efforts, aiming to realize continuous growth through
                ongoing developments and improvements.<br />
                <br />
                Our efforts, Vision, focus on clients’ satisfaction and continuous improvement plans
                are why we are different from our competitors.
            </p>
        </div>
        <div class="sidecontent">
            <div class="faq">
                <a href="~/FAQs.aspx" runat="server">
                    <h2>
                        F.A.Q</h2>
                    <ul class="faqlist">
                        <li>How do I open an account with Al Jazeera financial services ?</li>
                        <li>How do I give my orders to the Brokers at Al Jazeera ?</li>
                        <li>How do I know that my order has been filled either when buying shares or selling
                            them</li>
                        <li>How do I know that my order has been filled either when buying shares or selling
                            them</li>
                    </ul>
                </a>
            </div>
        </div>
    </div>
</asp:Content>
