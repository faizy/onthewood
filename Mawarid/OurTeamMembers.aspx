﻿<%@ Page Title="Mawarid | Our Team" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="OurTeamMembers.aspx.cs" Inherits="Mawarid.OurTeamMembers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">
    $(document).ready(function () {
        $("#lnkOurTeam").addClass("activeTop");
    });
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div class="banner">
        <img src="images/teambanner.jpg" alt="mawarid" />
    </div>
    <div class="aboutcontent">
        
        <div class="maincont">
            <h2>
                OUR TEAM</h2>
            <div class="clearfix">
            </div>
            <div class="teamcontainer">
                <div id="ca-container" class="ca-container">
                    <div class="ca-wrapper">
                        <div class="ca-item">
                            <asp:Repeater runat="server" ID="rptContent" OnItemCommand="rptContent_ItemCommand">
                                <ItemTemplate>
                                    <div class="teammember">
                                        <div class="teamimg">
                                            <asp:Image runat="server" ID="imgImage" ImageUrl='<%# ConfigurationManager.AppSettings["SiteUrl"]+"Uploads/"+Eval("Image") %>'
                                                CssClass="ourTeamImage" />
                                        </div>
                                        <div class="teamdesc">
                                            <div class="teamname">
                                                <%# Eval("Name") %>
                                            </div>
                                            <div class="job">
                                                <%# Eval("Designation") %></div>
                                            <div class="teaminfo">
                                                <%# Eval("Details").ToString().Length > 150 ? Eval("Details").ToString().Substring(0, 150) : Eval("Details").ToString()%>
                                            </div>
                                        </div>
                                        <div class="readmore">
                                            <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit" CommandArgument='<%# Eval("OurTeamId") %>'>READ MORE</asp:LinkButton>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>
            <div class="dline">
                <img src="images/dline.png" alt="MAWARID SECURTIES" />
            </div>
            <p style="clear: both;">
                The experiences of wise and highly skilled employees of Al Jazeera Financial Services
                include various disciplines which together form a major base for achieving success
                and providing superior service and the best quality and value for its customers.
                Al Jazeera’s employees are highly skilled, dynamic and speedy decision-makers. These
                skills and experiences it is diverse and a comprehensive source of distinct strategies
                always evolving, along with the distinct ability to implement these policies carefully
                and develop with the evolution of the company. "We are thoroughly combined of distinct
                experiences with diverse financial expertise and best international practices, which
                conform completely to the requirements of the local market.”<br />
                <br />
                Al Jazeera Financial Services is working to establish long-term relationships with
                a wide network of consultants and experts in employment sectors to benefit from
                their experience. Within the strategic vision of functional competencies in the
                process of continuous growth, Al Jazeera is committed to attracting the best skills
                and develops continuously and provides all means of support and support of the Operation
                Team to ensure progress.
            </p>
        </div>
        <div class="sidecontent">
            <div class="faq">
                <a href="~/FAQs.aspx" runat="server">
                    <h2>
                        F.A.Q</h2>
                    <ul class="faqlist">
                        <li>How do I open an account with Al Jazeera financial services ?</li>
                        <li>How do I give my orders to the Brokers at Al Jazeera ?</li>
                        <li>How do I know that my order has been filled either when buying shares or selling
                            them</li>
                        <li>How do I know that my order has been filled either when buying shares or selling
                            them</li>
                    </ul>
                </a>
            </div>
        </div>
    </div>
</asp:Content>
