﻿<%@ Page Title="Mawarid | Terms & Conditions" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="TermsAndConditions.aspx.cs" Inherits="Mawarid.TermsAndConditions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">
    $(document).ready(function () {
        $("#lnkTerms").addClass("active");
    });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div class="banner">
        <img src="images/TermsAndConditions.png" alt="mawarid" />
    </div>
    <div class="aboutcontent">
        <div class="maincont" style="width: 960px;">
            <h2>
                TERMS & CONDITIONS</h2>
            <div class="terms">
                Provision of <b>"Aljazeera Financial Services"</b> for execution of local shares
                trading transactions via the Internet is subject to the following Terms & Conditions:<br />
                In these Terms & Conditions, the following terms shall have the meanings indicated
                opposite thereto below:
                <br />
                <h3>
                    COMPANY</h3>
                <span>Means “Aljazeera Financial Services ". </span>
                <h3>
                    CUSTOMER or USER</h3>
                <span>Means any natural or juristic person who sets up an account under his/her name
                    with “Al Jazeera Financial Services” and subscribes to "Aljazeera - Financial Services"
                    Service.</span>
                <h3>
                    SERVICE</h3>
                <span>Means "online service" for execution of local shares trading transactions via
                    the Internet.</span>
                <ul>
                    <li>Local shares trading transactions via the Internet shall include those “Aljazeera
                        Financial Services” shall determine, it its sole discretion, to be covered in the
                        Service as it deems fit from time to time. The Service does not constitute an offer
                        for or solicitation to the Customer to subscribe to any of the investment services
                        provided by “Aljazeera Financial Services”.</li>
                    <li>The Customer hereby authorizes “Aljazeera Financial Services” to accept and act
                        upon electronic instructions submitted via the Internet and to respond to any Customer
                        inquiries so received. </li>
                    <li>The Customer acknowledges and consents that “Aljazeera Financial Services” has advised
                        him/her, and that he/she has taken note, of current technology restrictions and
                        limitations, limitations and restriction on “Al Jazeera - A Mawarid Finance Company”
                        Service , that Service availability without interruption depends on technology applications
                        and level of compliance with the restrictions on Service , and accordingly hereby
                        releases “Al Financial Services” from any and every liability which may arise as
                        a result of Customer's inability to use the Service for any reason whatsoever.
                    </li>
                    <li>Aljazeera Financial Services Company reserves the right to refuse to provide the
                        Service to any customer for any reason whatsoever, and to refuse to act upon or
                        respond to any instructions or queries it may receive from the Customer via the
                        Internet without stating any reasons. </li>
                    <li>All equipments advanced by Aljazeera Financial Services Company to the Customer
                        are and shall remain the exclusive property of Al Jazeera - A Mawarid Finance Company.
                        The Customer agrees and undertakes to return these equipment to Al Jazeera - A Mawarid
                        Finance Company immediately on demand in their original "as received" condition.
                    </li>
                    <li>The Customer consents and warrants that the Customer will not copy reproduce or
                        amend any software, pages or documents provided by Aljazeera Financial Services
                        Company nor will the Customer download or divert the Service software from any computer
                        or electronic device to any other computer or electronic device. </li>
                    <li>Either Aljazeera Financial Services or the Customer may cancel subscription to the
                        Service. If the Customer intends to cancel subscription, a written notice of cancellation
                        must be submitted to Aljazeera Financial Services Company. </li>
                    <li>Aljazeera Financial Services Company is entitled to cancel the Customer's subscription
                        at anytime without notice to the Customer, and such action shall not create any
                        liabilities on Aljazeera Financial Services Company’s part to the Customer.
                    </li>
                    <li>Aljazeera Financial Services Company does not accept liability for any losses, damages,
                        costs or expenses whatsoever or howsoever incurred or sustained by the Customer
                        as a result of breach of any of these terms and conditions. </li>
                    <li>Any costs incurred towards Internet usage to logon to the Service shall be for the
                        Customer's exclusive account. </li>
                    <li>The Customer authorizes the Aljazeera Financial Services Company to send Service
                        related advices, notices and/or correspondence in the manner the Aljazeera Financial
                        Services Company deems fit. The Customer shall be responsible for the confidentiality
                        of all Aljazeera Financial Services Company generated Customer advices, notices
                        and correspondence. </li>
                    <li>The Aljazeera Financial Services Company does not accept liability for any errors
                        or damages resulting from delayed or failed instructions, including;
                        <ul>
                            <li>Insufficient account balance to permit execution of the instructions. </li>
                            <li>Insufficient instructions to execute a transaction. </li>
                            <li>Failure to comply with the instructions or guidance given, or unclear or incomplete
                                Customer instructions. </li>
                            <li>Delay or any technical fault at the NSBs end or any external agency with which the
                                NSB does business. </li>
                        </ul>
                    </li>
                    <li>Aljazeera Financial Services Company reserves the right to refuse to execute any
                        transaction without notice or liability. </li>
                    <li>No failure or delay on Aljazeera Financial Services part to exercise any rights
                        or remedy under these Terms Conditions shall operate as a waiver of such right or
                        remedy, nor will any partial exercise of any right or remedy preclude any subsequent
                        exercise of such right or remedy. </li>
                    <li>Rights and remedies provided herein are supplementary and additional to any other
                        rights or remedies provided under the applicable law. </li>
                    <li>In no event will Aljazeera Financial Services or its employees be liable for any
                        damages, including without limitation direct or indirect, special, incidental, or
                        consequential damages, losses or expenses arising in connection with logon to the
                        software or use thereof or inability to use by any party, or in connection with
                        any failure of performance, error, omission, interruption, defect, delay in operation
                        or transmission, computer virus or line or system failure, even when Aljazeera Financial
                        Services, or representatives thereof, are advised of the possibility of such damages,
                        losses or expenses. </li>
                    <li>Aljazeera Financial Services reserves the right to alter these Terms & Conditions
                        at anytime, but will notify the customer of any alternation in advance of the effective
                        date. Continued of the Service by the customer constitutes acceptance and agreement
                        of such alterations. </li>
                    <li>These Terms & Conditions are supplementary to, and shall be governed by, any related
                        terms, conditions and/or agreements executed by the Customer, present or in the
                        future. </li>
                    <li>These Terms & Conditions are subject to and shall be governed by the prevailing
                        rules and regulations of the UAE. The parties hereby irrevocably submit to the exclusive
                        jurisdiction of the Committee for Settlement of Aljazeera Financial Services Company
                        disputes for resolution of any disputes arising there under. </li>
                    <li>The client shall admit that he has knowledge in market system, all legislations,
                        laws, and issued instruction by the Authority and markets and the competent parties
                        with the market dealings, and any other department related herein, or has an direct
                        or indirect impact on the market transactions, any amendment or law, legislations
                        lately issued, especially, regarding to online trading, subsequently, he shall be
                        committed to remunerate the mediator for any damages or losses, fines incurred herein,
                        as a result of not subject to the market systems, legislations, laws and the above
                        mentioned instructions while signing in to the online trading service. </li>
                    <li>The client shall admit and realize that this system may be intervened or breach
                        as per computer pirates or any third party, which may be led amendments procedures
                        or submitting false orders to clients, and shall admit that the company (Mediator)
                        is not liable for any remuneration, and all submitted order as per online system
                        entry, a binding and final password, and the company is not liable for any act.
                    </li>
                    <li>The client shall agree that his usage for that online trading service is under his
                        own responsibilities, and any of the company, directors, employees, or affiliated
                        companies or information providers, do not guaranteed offline services or free from
                        any mistakes. Also, the company have not liable for submitting any bail regarding
                        the outcomes, that can be acquired from using such site, or regarding the period
                        feasibility, accuracy, completion, or the content of any information or services,
                        or transactions provided by the site, or regarding any updating program for signing
                        in to the internet. </li>
                    <li>The client shall be aware of delay occurrences in implementing or deleting or amending
                        his orders, in addition to, these delays may be happened enormously during intensive
                        transactions or huge changes in prices (Rapid Market), so, the client shall be realized
                        that he will receive the price same as he agreed upon which may be differs from
                        the current price in the PC entry. 24- He has to be aware that those orders through
                        the net will have less priority than those orders issued during normal trading sessions
                        at the same price. </li>
                    <li>Certain orders by the client won’t be placed due to the non-compatibility of the
                        price with the minor max limit of trading or to cease the trading on particular
                        share by the market authority. </li>
                    <li>The client shall agree that the company cannot guarantee that his cancellation or
                        amended order must be implemented, in spite that he has submitted it before receiving
                        the notice, that the order is executed. </li>
                    <li>The client shall understand and agree that if it is not possible for canceling or
                        amending for the order, so he bended to executive original order as well as admit
                        that cancellation or amended trials, and substituting order or implementing a repeated
                        orders, and client shall be liable in executing all. </li>
                </ul>
                <h3>
                    ACKNOWLEDGMENTS
                </h3>
                <ul>
                    <li>The client shall admit his full legal responsibility or any resulted obligations
                        due to password forgetfulness or password use by others or unauthorized persons
                        from the date of receiving the password. </li>
                    <li>The client shall admit that the company holds no responsibility for any delay or
                        execution failure or amendment or cancellation or losing any order during the contacts
                        resulted from the system disorder for any reasons, such as program out of order
                        or virus occurrence, or any other factors that lead to order rejection. </li>
                    <li>The client shall admit the he is solely liable for the submitted doubled orders
                        through any online trading system, either deliberately or non-deliberately as for
                        the double register or order lost. </li>
                    <li>The client shall admit the he is solely liable for any resulted losses resulted
                        from the orders in a unsuitable method or the failure in filling the form in an
                        improper method. </li>
                    <li>The client shall admit that the Broker holds no responsibilities for any damages
                        or losses or faults incurred from any faults or inaccuracy or any delete in content
                        or the service or any delay or offline service for the client. </li>
                    <li>Customer acknowledges that he is aware that the performance and availability of
                        electronic trading system may be affected by factors such as increasing the number
                        of participants or any malfunction in computers or malfunction in the telephone
                        lines or Internet connection and other reasons, as he acknowledges his agreement
                        to use the electronic trading system and bear the responsibility for all risks resulting
                        from such use. The Client hereby declares and confirms that he is fully assure of
                        SCA and Emirates Financial Markets Rules and Regulations in the UAE, though the
                        client acknowledges the responsibility for any penalty (fine) which occurs on his
                        trading accounts the event he breaches these laws and regulations. Accordingly the
                        broker is not obligated for any financial or legal responsibility for these penalties
                        (fines).
                        <ul>
                            <li>The broker won’t give any guarantees concerning speed or efficiency of E-Trading
                                through the net. </li>
                            <li>In compatibility of the client’s personal computer system with the E-Trade system.
                            </li>
                            <li>Any other obligations or risks that weren’t mentioned above and that can be acknowledged
                                by the profession specialists. </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</asp:Content>
