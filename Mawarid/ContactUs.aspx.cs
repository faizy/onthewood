﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using MawaridBLL;

namespace Mawarid
{
    public partial class ContactUs : Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            var masterPage = Master;
            if (masterPage == null) return;
            var lnkContactUs = (HtmlAnchor)masterPage.FindControl("lnkContactUs");
            lnkContactUs.Attributes.Add("class", "active");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ShowRecord();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                SendEmail();
            }
            catch (Exception ex)
            {

            }
        }

        protected void ShowRecord()
        {
            var rep = new ContactUsRepository();
            var entity = rep.GetContactUsById();
            if (entity != null)
            {
                lblEmail.InnerText = entity.Email;
                lblMobile.InnerText = entity.Mobile;
                lblTel.InnerText = entity.Tel;
                lblWorkingHours.InnerText = entity.Working_Hours;
            }
        }

        #region PageMethods
        protected void SendEmail()
        {
            var mm = new MailMessage(txtEmail.Text.Trim(), ConfigurationManager.AppSettings["ADMIN_EMAIL"], txtFName.Text.Trim() + " has sent you a message.", MailBody())
            {
                IsBodyHtml = true
            };
            try
            {
                var smtp = new SmtpClient();
                smtp.Send(mm);
                mm.Dispose();
                smtp.Dispose();
                Response.Redirect("~/Confirmation.aspx", false);
            }
            catch (Exception ex)
            {


            }
        }
        protected string MailBody()
        {

            var reader = new StreamReader(Server.MapPath("~/ContactUs.html"));
            var body = reader.ReadToEnd();
            reader.Close();
            reader.Dispose();
            body = body.Replace("<name>", txtFName.Text.Trim() + " " + txtLName.Text.Trim());
            body = body.Replace("<email>", txtEmail.Text.Trim());
            body = body.Replace("<phone>", txtPhone.Text.Trim());
            body = body.Replace("<comment>", txtMessage.Text.Trim());
            return body;
        }
        #endregion
    }
}