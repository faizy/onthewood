﻿<%@ Page Title="Board of Directors Details" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="BoardOfDirectorsDetails.aspx.cs" Inherits="Mawarid.BoardOfDirectorsDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#lnkBoardOfDirectors").addClass("activeTop");
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div class="banner">
        <img src="images/teambanner.jpg" alt="mawarid" />
    </div>
    <div class="aboutcontent">
        <div class="maincont">
            <h2>
                BOARD OF DIRECTORS</h2>
            <div class="backbtn">
                <a href="~/BoardOfDirectors.aspx" runat="server">BACK</a>
            </div>
            <div class="clearfix">
            </div>
            <h3 id="lblName" runat="server">
            </h3>
            <h4 id="lblDesignation" runat="server">
            </h4>
            <p>
                <span class="profileimage">
                    <asp:Image runat="server" ID="imgProfile" CssClass="ourTeamImage" />
                </span><span id="lblDetails" runat="server" class="capDetail"></span>
            </p>
        </div>
        <div class="sidecontent">
            <div class="faq">
                <a id="A2" href="~/FAQs.aspx" runat="server">
                    <h2>
                        F.A.Q</h2>
                    <ul class="faqlist">
                        <li>How do I open an account with Al Jazeera financial services ?</li>
                        <li>How do I give my orders to the Brokers at Al Jazeera ?</li>
                        <li>How do I know that my order has been filled either when buying shares or selling
                            them</li>
                        <li>How do I know that my order has been filled either when buying shares or selling
                            them</li>
                    </ul>
                </a>
            </div>
        </div>
    </div>
</asp:Content>
