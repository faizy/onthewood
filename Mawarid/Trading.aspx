﻿<%@ Page Title="Mawarid | Trading" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Trading.aspx.cs" Inherits="Mawarid.Trading" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#lnkTrading").addClass("activeTop");
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div class="banner">
        <img src="images/aboutbanner.jpg" alt="mawarid" />
    </div>
    <div class="aboutcontent">
        <div class="maincont">
            <h2>
                TRADING</h2>
            <div class="clearfix">
            </div>
            <p>
                Trading has never been easier with Aljazeera, once you have establishes a Trading
                account with us, kindly contact us through the different means of communication:
                Phone Calls (Company records them as SAC requirement), Written, Orders, E-mails
                (Designated Dealing Room’s E-Mails addresses provided by the company), Faxes (Designated
                Dealing Fax Numbers provided by the company).
                <br />
                Customers can follow up on the market through our web-site on-time updated prices
                and volumes, and take a Trading decision accordingly.
                <br />
                Once Executed an SMS message will be sent to the client’s provided mobile number,
                and E-mails will be sent to the granted E-mails address.
            </p>
            <h3>
                Online trading:</h3>
            <p>
                Once interested in Online Trading, you can kindly sign the E-Trade section in the
                provided Account Opening Form, and an E-Trade’s User Name will be created in addition
                to a password and they both will be sent to your preferable point of contact (mainly
                Mobiles).
            </p>
            <h3>
                Link for <a href="http://etrade.jfs.ae/clients/index.php" target="new">E-Trade</a></h3>
        </div>
        <div class="sidecontent">
            <div class="faq">
                <a href="~/FAQs.aspx" runat="server">
                    <h2>
                        F.A.Q</h2>
                    <ul class="faqlist">
                        <li>How do I open an account with Al Jazeera financial services ?</li>
                        <li>How do I give my orders to the Brokers at Al Jazeera ?</li>
                        <li>How do I know that my order has been filled either when buying shares or selling
                            them</li>
                        <li>How do I know that my order has been filled either when buying shares or selling
                            them</li>
                    </ul>
                </a>
            </div>
        </div>
    </div>
</asp:Content>
