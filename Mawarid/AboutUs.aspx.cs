﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Mawarid
{
    public partial class AboutUs : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            var masterPage = Master;
            if (masterPage == null) return;
            var lnkAboutUs = (HtmlAnchor)masterPage.FindControl("lnkAboutUs");
            lnkAboutUs.Attributes.Add("class", "active");
        }
    }
}