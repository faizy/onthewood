﻿using System;
using System.Configuration;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using MawaridBLL;

namespace Mawarid.Moderator
{
    public partial class Contactus : Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    InitializePage();
                }
            }
            catch (Exception ex)
            {
                        
            }
        }

        protected void InitializePage()
        {
            var dRep = new ContactUsRepository();
            var entity = dRep.GetContactUsById();
            if (entity != null)
            {
                txtEmail.Text = entity.Email;
                txtMobile.Text = entity.Mobile;
                txtOpening.Text = entity.Working_Hours;
                txtphone.Text = entity.Tel;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                ManageContactUs();
            }
            catch (Exception ex)
            {

            }
        }

        protected void ManageContactUs()
        {
            var entity = new ContactU();
            var rep = new ContactUsRepository();
            entity.Email = txtEmail.Text.Trim();
            entity.Tel = txtphone.Text.Trim();
            entity.Mobile = txtMobile.Text.Trim();
            entity.Working_Hours = txtOpening.Text.Trim();
            rep.UpdateContactUs(entity);
        }       
    }
}