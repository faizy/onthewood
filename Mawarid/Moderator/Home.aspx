﻿<%@ Page Title="Home" Language="C#" MasterPageFile="~/Moderator/Moderator.Master"
    AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Mawarid.Moderator.Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div id="content">
        <div id="main">
            <div class="half_w half_right" style="clear: both; min-width: 897px;">
                <div class="h_title">
                    WELCOME</div>
                <div class="stats">
                    <div class="week">
                        <h1 style="padding: 60px; color: #04664D;">
                            Welcome to Mawarid Securities Content Management System.</h1>
                    </div>
                </div>
            </div>
            <div class="clear">
            </div>
        </div>
    </div>
</asp:Content>
