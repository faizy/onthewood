﻿using System;
using System.Configuration;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using MawaridBLL;

namespace Mawarid.Moderator
{
    public partial class FilesManagementPage : Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    InitializePage();
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void InitializePage()
        {
            var dRep = new FilesRepository();
            var dList = dRep.GetAllFilesManagement();
            if (dList.Count > 0)
            {
                rptContent.DataSource = dList;
                rptContent.DataBind();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                ManageFiles();
            }
            catch (Exception ex)
            {

            }
        }

        protected void rptContent_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            try
            {
                var rep = new FilesRepository();
                if (e.CommandName == "Edit")
                {
                    var entity = rep.GetFilesManagementById(Convert.ToInt32(e.CommandArgument));
                    if (entity != null)
                    {
                         hfBoardOfDirectorId.Value = e.CommandArgument.ToString();
                         txtName.Text = entity.Name;
                         fuImage.Visible = false;
                         hplImage.Visible = false;
                         lblAttachment.Visible = false;
                         lblMpdified.Visible = true;
                         txtModifiedDate.Visible = true;
                         txtModifiedDate.Text = string.Format("{0:MM/dd/yyyy}", entity.AddedDate);
                    
                    }
                }
                if (e.CommandName == "Delete")
                {
                    rep.DeleteFilesManagement(Convert.ToInt32(e.CommandArgument));
                    InitializePage();
                }
            }
            catch (Exception ex)
            {

            }
        }
      

        protected void ManageFiles()
        {
            var entity = new FilesManagement();
            var rep = new FilesRepository();
            entity.Name = txtName.Text.Trim();
            if (fuImage != null)
            {
                entity.Attachment = SaveFile(ref fuImage);
            }

            if (hfBoardOfDirectorId.Value != "")
            {
                entity.FileManagementId = Convert.ToInt32(hfBoardOfDirectorId.Value);
                entity.AddedDate = Convert.ToDateTime(txtModifiedDate.Text.Trim());
                rep.UpdateFilesManagement(entity);
            }
            else
            {
            entity.FileManagementId = 0;
            entity.AddedDate = DateTime.Now.Date;
            rep.InsertFilesManagement(entity);
            }
            InitializePage();
            txtName.Text = "";
            txtModifiedDate.Text = "";
            hplImage.Visible = false;
        }

        protected string SaveFile(ref FileUpload fu)
        {
            string fileName = "N/A";
            var fileExt = "";
            if (fu.HasFile)
            {
                var guid = Guid.NewGuid();
                var guidFirst = guid.ToString().Split('-');
                fileName = fu.PostedFile.FileName;
                fileExt = Path.GetExtension(fileName);
                fileName = guidFirst[4];
                var uplaodPath = Server.MapPath("~/Uploads/" + fileName + fileExt);
                fu.SaveAs(uplaodPath);
            }
            return fileName + fileExt;
        }
    }
}