﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Mawarid.Moderator.Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Login</title>
    <link href="css/login.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="wrap">
        <div id="content">
            <div id="main">
                <div class="full_w">
                    <label for="login">
                        Username:</label>
                    <input id="login" name="login" class="text" />
                    <label for="pass">
                        Password:</label>
                    <input id="pass" name="pass" type="password" class="text" />
                    <div class="sep">
                    </div>
                    <asp:Button runat="server" ID="btnSubmit" CssClass="ok" Text="LOGIN" OnClick="btnSubmit_Click" />
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
