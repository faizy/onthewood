﻿<%@ Page Title="Contact Us" Language="C#" MasterPageFile="~/Moderator/Moderator.Master"
    AutoEventWireup="true" CodeBehind="Contactus.aspx.cs" Inherits="Mawarid.Moderator.Contactus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div id="content">
        <div id="main">
            <div class="full_w">
                <div class="h_title">
                    MANAGE CONTACT US</div>
                <div class="element mtop25">
                    <div class="controlsDiv">
                        <label>
                            Email:
                        </label>
                        <asp:TextBox runat="server" ID="txtEmail" CssClass="text"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ID="rftxtEmail" ControlToValidate="txtEmail"
                            Display="Dynamic" ValidationGroup="validate" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </div>
                    <div class="controlsDiv">
                        <label>
                            Land Line:
                        </label>
                        <asp:TextBox runat="server" ID="txtphone" CssClass="text"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ID="rftxtphone" ControlToValidate="txtphone"
                            Display="Dynamic" ValidationGroup="validate" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="element mtop25">
                    <div class="controlsDiv">
                        <label>
                            Mobile:
                        </label>
                        <asp:TextBox runat="server" ID="txtMobile" CssClass="text"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ID="rftxtMobile" ControlToValidate="txtMobile"
                            Display="Dynamic" ValidationGroup="validate" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </div>
                    <div class="controlsDiv">
                        <label>
                            Opening Day:
                        </label>
                        <asp:TextBox runat="server" ID="txtOpening" CssClass="text"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ID="rftxtOpening" ControlToValidate="txtOpening"
                            Display="Dynamic" ValidationGroup="validate" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="entry mtop25">
                    <asp:Button runat="server" ID="btnSubmit" Text="SAVE" ValidationGroup="validate"
                        CssClass="add button" Style="margin-left: 320px;" OnClick="btnSubmit_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
