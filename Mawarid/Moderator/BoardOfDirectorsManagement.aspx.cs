﻿using System;
using System.Configuration;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using MawaridBLL;

namespace Mawarid.Moderator
{
    public partial class BoardOfDirectorsManagement : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    InitializePage();
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void InitializePage()
        {
            var dRep = new BoardOfDirectorsRepository();
            var dList = dRep.GetAllBoardOfDirector();
            if (dList.Count > 0)
            {
                rptContent.DataSource = dList;
                rptContent.DataBind();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                ManageBoardOfDirectors();
            }
            catch (Exception ex)
            {

            }
        }

        protected void rptContent_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            try
            {
                var rep = new BoardOfDirectorsRepository();
                if (e.CommandName == "Edit")
                {
                    var entity = rep.GetBoardOfDirectorById(Convert.ToInt32(e.CommandArgument));
                    if (entity != null)
                    {
                        hfBoardOfDirectorId.Value = e.CommandArgument.ToString();
                        txtName.Text = entity.Name;
                        txtDesignation.Text = entity.Designation;
                        txtDetails.Text = entity.Details;
                        lblMpdified.Visible = true;
                        txtModifiedDate.Visible = true;
                        txtModifiedDate.Text = string.Format("{0:MM/dd/yyyy}", entity.ModifiedDate);
                        hfImage.Value = entity.Image;
                        hplImage.NavigateUrl = ConfigurationManager.AppSettings["SiteUrl"] + "Uploads/" + entity.Image;
                        hplImage.Visible = true;
                    }
                }
                else if (e.CommandName == "Delete")
                {
                    rep.DeleteBoardOfDirector(Convert.ToInt32(e.CommandArgument));
                    InitializePage();
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void ManageBoardOfDirectors()
        {
            var entity = new BoardOfDirector();
            var rep = new BoardOfDirectorsRepository();
            entity.Name = txtName.Text.Trim();
            entity.Designation = txtDesignation.Text.Trim();
            entity.Details = txtDetails.Text.Trim();
            entity.RecordStatus = Utilities.RecordStatus.Active;
            if (fuImage.HasFile)
            {

                entity.Image = SaveFile(ref fuImage);
            }
            else
            {
                entity.Image = hfImage.Value;
            }
            if (hfBoardOfDirectorId.Value != "")
            {
                entity.BoardOfDirectorId = Convert.ToInt32(hfBoardOfDirectorId.Value);
                entity.ModifiedDate = Convert.ToDateTime(txtModifiedDate.Text.Trim());
                rep.UpdateBoardOfDirector(entity);
            }
            else
            {
                entity.BoardOfDirectorId = 0;
                rep.InsertBoardOfDirector(entity);
            }
            InitializePage();
            txtName.Text = "";
            txtDesignation.Text = "";
            txtDetails.Text = "";
            hfBoardOfDirectorId.Value = "";
            txtModifiedDate.Text = "";
            hplImage.Visible = false;
        }

        protected string SaveFile(ref FileUpload fu)
        {
            string fileName = "N/A";
            var fileExt = "";
            if (fu.HasFile)
            {
                var guid = Guid.NewGuid();
                var guidFirst = guid.ToString().Split('-');
                fileName = fu.PostedFile.FileName;
                fileExt = Path.GetExtension(fileName);
                fileName = guidFirst[4];
                var uplaodPath = Server.MapPath("~/Uploads/" + fileName + fileExt);
                fu.SaveAs(uplaodPath);
            }
            return fileName + fileExt;
        }
    }
}