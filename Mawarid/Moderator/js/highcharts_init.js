$(function () {
    var chart;
    $(document).ready(function () {
        Highcharts.setOptions({
            colors: ['#74B654']
        });
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container',
                type: 'column',
                margin: [50, 30, 80, 60]
            },
            title: {
                text: 'ORDERS THIS MONTH'
            },
            xAxis: {
                categories: [
					'11-03-2014',
                    '12-03-2014',
                    '13-03-2014',
                    '14-03-2014',
                    '15-03-2014',
                    '16-03-2014',
                    '17-03-2014',
                    '18-03-2014',
                    '19-03-2014',
                    '20-03-2014',
                    '21-03-2014',
                    '22-03-2014',
                    '23-03-2014',
                    '24-03-2014',
                    '25-03-2014',
                    '26-03-2014',
                    '27-03-2014',
                    '28-03-2014',
                    '29-03-2014'
                ],
                labels: {
                    rotation: -45,
                    align: 'right',
                    style: {
                        fontSize: '9px',
                        fontFamily: 'Tahoma, Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Orders'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.x + '</b><br/>' +
                        'Orders: ' + Highcharts.numberFormat(this.y, 0);
                }
            },
            series: [{
                name: 'Orders',
                data: [1134, 1029, 1626, 2210, 1019, 1209, 2319, 4118, 1418,
                    1127, 1465, 1375, 1026, 1654, 1423, 1142, 3312, 1126],
                dataLabels: {
                    enabled: false,
                    rotation: 0,
                    color: '#4FAC3A',
                    align: 'right',
                    x: -3,
                    y: 20,
                    formatter: function () {
                        return this.y;
                    },
                    style: {
                        fontSize: '11px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                },
                pointWidth: 20
            }]
        });
    });

});
