﻿<%@ Page Title="Our Team" Language="C#" MasterPageFile="~/Moderator/Moderator.Master"
    AutoEventWireup="true" CodeBehind="OurTeamManagement.aspx.cs" Inherits="Mawarid.Moderator.OurTeamManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/jquery-ui.js" type="text/javascript"></script>
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {
            $("#body_txtModifiedDate").datepicker();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div id="content">
        <div id="main">
            <div class="full_w">
                <div class="h_title">
                    MANAGE OUR TEAM</div>
                <div class="element mtop25">
                    <div class="controlsDiv">
                        <label>
                            Name:
                        </label>
                        <asp:TextBox runat="server" ID="txtName" CssClass="text"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ID="rftxtName" ControlToValidate="txtName"
                            Display="Dynamic" ValidationGroup="validate" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </div>
                    <div class="controlsDiv">
                        <label>
                            Designation:
                        </label>
                        <asp:TextBox runat="server" ID="txtDesignation" CssClass="text"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ID="rftxtDesignation" ControlToValidate="txtDesignation"
                            Display="Dynamic" ValidationGroup="validate" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="element">
                    <div class="controlsDiv">
                        <label>
                            Attachment:
                        </label>
                        <asp:FileUpload runat="server" ID="fuImage" />
                        <asp:HiddenField runat="server" ID="hfImage" />
                        <asp:HyperLink runat="server" ID="hplImage" Visible="False" Target="_blank">View</asp:HyperLink>
                    </div>
                    <div class="controlsDiv">
                        <label id="lblMpdified" runat="server" visible="false">
                            Modified Date:
                        </label>
                        <asp:TextBox runat="server" ID="txtModifiedDate" CssClass="text" Visible="false"></asp:TextBox>
                        <%--<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtDesignation"
                            Display="Dynamic" ValidationGroup="validate" ErrorMessage="*"></asp:RequiredFieldValidator>--%>
                    </div>
                </div>
                <div class="element">
                    <div class="controlsDiv" style="width: 100%;">
                        <label>
                            Details:
                        </label>
                        <asp:TextBox runat="server" ID="txtDetails" CssClass="text" Style="width: 72.5%;"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ID="rftxtDetails" ControlToValidate="txtDetails"
                            Display="Dynamic" ValidationGroup="validate" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="entry mtop25">
                    <asp:HiddenField runat="server" ID="hfBoardOfDirectorId" Value="" />
                    <asp:Button runat="server" ID="btnSubmit" Text="SAVE" ValidationGroup="validate"
                        CssClass="add button" Style="margin-left: 320px;" OnClick="btnSubmit_Click" />
                </div>
            </div>
            <div class="full_w">
                <div class="h_title">
                    Search</div>
                <div class="entry">
                    <a id="A1" class="button add" href="~/Moderator/OurTeamManagement.aspx" title="Add a new patient"
                        runat="server">ADD NEW</a>
                    <div class="sep">
                    </div>
                </div>
                <table>
                    <thead>
                        <tr>
                            <th scope="col">
                                Name
                            </th>
                            <th scope="col">
                                Designation
                            </th>
                            <th scope="col">
                                Image
                            </th>
                            <th scope="col">
                                Modified Date
                            </th>
                            <th class="width10">
                                Edit
                            </th>
                            <th class="width10">
                                Delete
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater runat="server" ID="rptContent" OnItemCommand="rptContent_ItemCommand">
                            <ItemTemplate>
                                <tr>
                                    <td class="align-center">
                                        <%# Eval("Name") %>
                                    </td>
                                    <td class="align-center">
                                        <%# Eval("Designation") %>
                                    </td>
                                    <td class="align-center">
                                        <asp:Image runat="server" ID="imgImage" ImageUrl='<%# ConfigurationManager.AppSettings["SiteUrl"]+"Uploads/"+Eval("Image") %>'
                                            Style="width: 50px;" />
                                    </td>
                                    <td class="align-center">
                                        <%# Eval("ModifiedDate","{0:d}") %>
                                    </td>
                                    <td class="width10">
                                        <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit" CommandArgument='<%# Eval("OurTeamId") %>'><img src="img/edit.png" alt="Edit" style="width: 35px;"/></asp:LinkButton>
                                    </td>
                                    <td class="width10">
                                        <asp:LinkButton runat="server" ID="lnkDelete" CommandName="Delete" CommandArgument='<%#Eval("OurTeamId")%>'
                                            OnClientClick="return confirm('Are you sure you want to delete this member?')"><img src="img/delete.png" alt="Edit" style="width: 35px;"/></asp:LinkButton>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
                <div class="entry">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
