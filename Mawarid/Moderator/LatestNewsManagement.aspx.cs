﻿using System;
using System.Configuration;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using MawaridBLL;

namespace Mawarid.Moderator
{
    public partial class LatestNewsManagement : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    InitializePage();
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void InitializePage()
        {
            var dRep = new LatestNewsRepository();
            var dList = dRep.GetAllLatestNews();
            if (dList.Count > 0)
            {
                rptContent.DataSource = dList;
                rptContent.DataBind();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                ManageLatestNews();
            }
            catch (Exception ex)
            {

            }
        }

        protected void rptContent_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            try
            {
                var rep = new LatestNewsRepository();
                if (e.CommandName == "Edit")
                {
                    var entity = rep.GetLatestNewsById(Convert.ToInt32(e.CommandArgument));
                    if (entity != null)
                    {
                        hfLatestNewsId.Value = e.CommandArgument.ToString();
                        txtName.Text = entity.Title;
                        txtDescription.Text = entity.Description;                       
                        hfImage.Value = entity.Image;
                        lblMpdified.Visible = true;
                        txtModifiedDate.Visible = true;
                        txtModifiedDate.Text = string.Format("{0:MM/dd/yyyy}", entity.ModifiedDate);
                        hplImage.NavigateUrl = ConfigurationManager.AppSettings["SiteUrl"] + "Uploads/" + entity.Image;
                        hplImage.Visible = true;
                    }
                }
                else if (e.CommandName == "Delete")
                {
                    rep.DeleteLatestNews(Convert.ToInt32(e.CommandArgument));
                    InitializePage();
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void ManageLatestNews()
        {
            var entity = new LatestNew();
            var rep = new LatestNewsRepository();
            entity.Title = txtName.Text.Trim();
            entity.Description = txtDescription.Text.Trim();            
            entity.RecordStatus = Utilities.RecordStatus.Active;
            if (fuImage.HasFile)
            {

                entity.Image = SaveFile(ref fuImage);
            }
            else
            {
                entity.Image = hfImage.Value;
            }
            if (hfLatestNewsId.Value != "")
            {
                entity.NewsId = Convert.ToInt32(hfLatestNewsId.Value);
                entity.ModifiedDate = Convert.ToDateTime(txtModifiedDate.Text.Trim());
                rep.UpdateLatestNews(entity);
            }
            else
            {
                entity.NewsId = 0;
                rep.InsertLatestNews(entity);
            }
            InitializePage();
            txtName.Text = "";
            txtDescription.Text = "";            
            hfLatestNewsId.Value = "";
            txtModifiedDate.Text = "";
            hplImage.Visible = false;
        }

        protected string SaveFile(ref FileUpload fu)
        {
            string fileName = "N/A";
            var fileExt = "";
            if (fu.HasFile)
            {
                var guid = Guid.NewGuid();
                var guidFirst = guid.ToString().Split('-');
                fileName = fu.PostedFile.FileName;
                fileExt = Path.GetExtension(fileName);
                fileName = guidFirst[4];
                var uplaodPath = Server.MapPath("~/Uploads/" + fileName + fileExt);
                fu.SaveAs(uplaodPath);
            }
            return fileName + fileExt;
        }
    }
}