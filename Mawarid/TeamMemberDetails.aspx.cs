﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using MawaridBLL;

namespace Mawarid
{
    public partial class TeamMemberDetails : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            var masterPage = Master;
            if (masterPage == null) return;
            var lnkOurTeam = (HtmlAnchor)masterPage.FindControl("lnkOurTeam");
            lnkOurTeam.Attributes.Add("class", "active");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    InitializePage();
                }
            }
            catch (Exception ex)
            {
                    
            }
        }

        protected void InitializePage()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                var rep = new OurTeamRepository();
                var entity = rep.GetOurTeamById(Convert.ToInt32(Utilities.Helper.Decrypt(Request.QueryString["id"])));
                if (entity != null)
                {
                    lblName.InnerHtml = entity.Name;
                    lblDesignation.InnerHtml = entity.Designation;
                    lblDetails.InnerHtml = entity.Details;
                    imgProfile.ImageUrl = ConfigurationManager.AppSettings["SiteUrl"] + "Uploads/" + entity.Image;
                }
            }
        }

    }
}