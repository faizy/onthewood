﻿<%@ Page Title="Mawarid | Board of Directors" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="BoardOfDirectors.aspx.cs" Inherits="Mawarid.BoardOfDirectors" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#lnkBoardOfDirectors").addClass("activeTop");
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div class="banner">
        <img src="~/images/directorbanner.jpg" alt="mawarid" runat="server" />
    </div>
    <div class="aboutcontent">
        <div class="maincont">
            <h2>
           
                BOARD OF DIRECTORS
            </h2>
            <div class="clearfix">
            </div>
            <p>
                Al Jazeera Financial Services (LLC) Board of Directors after Mawarid Finance collaboration:
            </p>
            <div class="teamcontainer">
                <asp:Repeater runat="server" ID="rptContent" OnItemCommand="rptContent_ItemCommand">
                    <ItemTemplate>
                        <div class="boardmember">
                            <div class="teamimg">
                                <asp:Image runat="server" ID="imgImage" ImageUrl='<%# ConfigurationManager.AppSettings["SiteUrl"]+"Uploads/"+Eval("Image") %>'
                                    CssClass="ourTeamImage" /></div>
                            <div class="teamname">
                                <%# Eval("Name") %>
                            </div>
                            <div class="job">
                                <%# Eval("Designation") %></div>
                            <div class="teaminfo">
                                <%# Eval("Details").ToString().Length > 150 ? Eval("Details").ToString().Substring(0, 150) : Eval("Details").ToString()%>
                            </div>
                            <div class="readmore">
                                <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit" CommandArgument='<%# Eval("BoardOfDirectorId") %>'>READ MORE</asp:LinkButton></div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
        <div class="sidecontent">
            <div class="faq">
                <a href="~/FAQs.aspx" runat="server">
                    <h2>
                        F.A.Q</h2>
                    <ul class="faqlist">
                        <li>How do I open an account with Al Jazeera financial services ?</li>
                        <li>How do I give my orders to the Brokers at Al Jazeera ?</li>
                        <li>How do I know that my order has been filled either when buying shares or selling
                            them</li>
                        <li>How do I know that my order has been filled either when buying shares or selling
                            them</li>
                    </ul>
                </a>
            </div>
        </div>
    </div>
</asp:Content>
