﻿<%@ Page Title="Mawarid | Online Trading Guide" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="OnlineTradingGuide.aspx.cs" Inherits="Mawarid.OnlineTradingGuide" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#lnkOnlineTradingGuide").addClass("activeTop");
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div class="banner">
        <img src="images/teambanner.jpg" alt="mawarid" />
    </div>
    <div class="aboutcontent">
        <div class="maincont">
            <h2>
                ONLINE TRADING GUIDE</h2>
            <div class="clearfix">
            </div>
            <p>
                We are currently in contact with the Trade system Vendor to provide us with the
                E-Trade Manual, so we will provide that later.
            </p>
            <h3>
                Fees and charge:</h3>
            <p>
                The commission is 0.00275 (0.275%) with a minimum amount of is 65 dirham plus 10
                dirham to the market and it is divided in to:<br />
                <br />
                Broker : 0.0015<br />
                <br />
                Market : 0.0005<br />
                <br />
                The Authority : 0.00025<br />
                <br />
                The Clearing : 0.0005<br />
                <br />
                Broker : 30 dirham<br />
                <br />
                Market : 20 dirham<br />
                <br />
                The Authority : 5 dirham<br />
                <br />
                The Clearing : 10 dirham<br />
                <br />
            </p>
            <h3>
                Stock market links:</h3>
            <br />
            <p>
                <span class="greentxt">DFM Market :</span> <a href="http://www.dfm.ae" target="new">
                    www.dfm.ae</a>
                <br />
                <br />
                <span class="greentxt">ADX Market:</span> <a href="http://www.adx.ae" target="new">www.adx.ae</a></p>
            <h3>
                How to open account:</h3>
            <p>
                You can trade shares of companies that their activities match the Islamic Shariah
                by opening an account and issuing an Investor Number at the Dubai Financial Market
                or Abu Dhabi Financial Market or both, and then open an account at Al Jazeera Financial
                Services. This service is provided by Al Jazeera Financial Services for individuals
                and companies from inside and outside the UAE. The following general conditions
                and procedures for opening an account and identification papers required of the
                investor.</p>
            <asp:Repeater runat="server" ID="rptContent">
                <ItemTemplate>
                    <div class="pdf">
                        <img src="images/download.png" alt="download the file" />
                        <asp:HyperLink runat="server" ID="hplAttachment" NavigateUrl='<%# ConfigurationManager.AppSettings["SiteUrl"]+"Uploads/"+Eval("Attachment") %>'
                            Target="_blank" Text='<%# Eval("Name") %>'>   
                        </asp:HyperLink>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <div class="sidecontent">
            <div class="faq">
                <a href="~/FAQs.aspx" runat="server">
                    <h2>
                        F.A.Q</h2>
                    <ul class="faqlist">
                        <li>How do I open an account with Al Jazeera financial services ?</li>
                        <li>How do I give my orders to the Brokers at Al Jazeera ?</li>
                        <li>How do I know that my order has been filled either when buying shares or selling
                            them</li>
                        <li>How do I know that my order has been filled either when buying shares or selling
                            them</li>
                    </ul>
                </a>
            </div>
        </div>
    </div>
</asp:Content>
