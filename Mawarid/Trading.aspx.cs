﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Mawarid
{
    public partial class Trading : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            var masterPage = Master;
            if (masterPage == null) return;
            var lnkTrading = (HtmlAnchor)masterPage.FindControl("lnkTrading");
            lnkTrading.Attributes.Add("class", "active");
        }
    }
}