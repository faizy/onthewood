﻿<%@ Page Title="Mawarid | FAQs" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="FAQs.aspx.cs" Inherits="Mawarid.FAQs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div class="banner">
        <img src="images/directorbanner.jpg" alt="mawarid" />
    </div>
    <div class="aboutcontent">
        
        <div class="faqcontent">
            <h2>
                Frequently Asked Questions ?</h2>
            <ul>
                <li class="que">How do I open an account with AL JAZEERA FINANCIAL SERVICES?</li>
                <li class="ans">For opening an account with AL JAZEERA the following documents are required:
                    <ul class="innerlist">
                        <li>You need to have an investor number with ABU DHABI FIANCIAL MARKET and DUBAI FINANCIAL
                            MARKET, Aljazeera can assist in that by providing the required forms and following
                            up with markets until issuance, Valid passport copy ( Original should be also brought),
                            Any other Valid identification (U.A.E. ID, Driving License),</li>
                        <li>Copy of Family Book (UAE LOCALS) & Copy of residence visa (UAE CITIZENS), Copy of
                            valid Power of attorney ( If found),</li>
                        <li>Valid Passport Copy of the second party (If power of attorney found),</li>
                        <li>If all the required document are presented you need then to fill the opening account
                            form with AL JAZEERA FINANCIAL SERVICES after reading and accepting all the terms
                            in it, </li>
                        <li>After filling the opening account form you will receive your account number with
                            AL JAZEERA FINANCIAL SERVICES.</li>
                    </ul>
                </li>
                <li class="que">How do I give my orders to the Brokers at AL JAZEERA?</li>
                <li class="ans">You may fill your orders in five different ways
                    <ul class="innerlist">
                        <li>Through Online trading system </li>
                        <li>Through calling our main office, </li>
                        <li>By email </li>
                        <li>By fax</li>
                        <li>If you are in one of our branches you can fill either the buying or selling order.</li>
                    </ul>
                </li>
                <li class="que">How do I know that my order has been executed either when buying shares
                    or selling them?</li>
                <li class="ans">If your order is executed, you will receive a SMS from AL JAZEERA FINANCIAL
                    SERVICES stating that your order has been either fully or partially executed,
                </li>
                <li class="que">How can I transfer my shares from other broker or from the clearance
                    to my account with ALJAZEERA?</li>
                <li class="ans">In both markets (DFM and ADSM), there is form for transferring shares
                    available in both markets you need to fill this form and send it to us so that we
                    can then continue with the procedures to bring your shares to your account with
                    us (Forms are available on Aljazeera’s Web-site and in office). </li>
                <li class="que">How is the commission calculated?</li>
                <li class="ans">The commission is 0.00275 (0.275%) with a minimum amount of is 65 dirham
                    plus 10 dirham to the market and it is divided in to: 0.0015 (Broker), 0.0005 (Market),
                    0.00025 (The Authority), 0.0005 (The clearing) 30 dirham ( Broker) , 20 dirham (
                    Market) , 5 dirham ( The Authority) ,10 dirham ( The clearing ). </li>
                <li class="que">When I will receive my check after selling my shares?</li>
                <li class="ans">According to the SCA and Market rules it takes two working days for
                    you to receive your check.</li>
                <li class="que">Where are your offices located?</li>
                <li class="ans">A- The main office located in Sheik Ziad Rd., Ibri Building, in front
                    of Nour Islamic Bank Metro Station, Mezzanine Floor.</li>
                <li class="que">Is there a minimum amount that should be kept in my account?</li>
                <li class="ans">No, we do not require a certain amount to be available in your account.
                    Even if the balance is Zero, you will not be charged. </li>
                <li class="que">What is the Currency used to trade in DFM & ADSM? </li>
                <li class="ans">Only AED (United Arab Emirates - Dirham) is used for trading in DFM
                    & ADSM</li>
                <li class="que">Have forgotten my online trading password, what can I do? </li>
                <li class="ans">You can contact any of our customer service executives and they will
                    help you reset your user name and password</li>
                <li class="que">Can I issue a cheque with another person’s name?</li>
                <li class="ans">No, cheques are only issued with the investor's name.</li>
                <li class="que">What is the minimum value for trading stocks?</li>
                <li class="ans">There is no minimum value for trading.</li>
            </ul>
        </div>
    </div>
</asp:Content>
