﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using MawaridBLL;

namespace Mawarid
{
    public partial class Site : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    InitializePage();
                }
            }
            catch (Exception ex)
            {
                
            }
        }

        protected void InitializePage()
        {
            var dRep = new LatestNewsRepository();
            var dList = dRep.GetAllLatestNews().Take(4);
            rptContent.DataSource = dList;
            rptContent.DataBind();
        }

        protected void rptContent_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {

            }
        }
      public  void LinkButton_Command(Object sender, CommandEventArgs e)
        {   
            var dRep = new LatestNewsRepository();
            var linesAfterTarget = dRep.GetAllLatestNews()
                  .Skip(1)
                  .Take(4)
                  .ToList();
            rptContent.DataSource = linesAfterTarget;
            rptContent.DataBind();
        }
    }
}