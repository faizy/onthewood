﻿<%@ Page Title="Mawarid | Home" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="Mawarid.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <script type="text/javascript">
     $(document).ready(function () {
         $("#lnkHome").addClass("active");
     });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div class="banner">
        <img src="~/images/banner.jpg" alt="mawarid" runat="server" />
    </div>
    <div>
        <div class="row1">
            <div class="openaccount">
                <h2>
                    HOW TO OPEN AN ACCOUNT ?</h2>
                <p>
                    You can share trades shares of the companies that their activities match the Islamic
                    Shariah by opening an account at the Dubai Financial Market or Abu-Dhabi financial
                    market or both and the open an account at Al Jazeera financial services.
                    <br />
                    <br />
                    The service is provided by Al Jazeera financial services for individuals and companies
                    from inside and outside.
                </p>
                <div class="readmore">
                    <a href="javascript:void(0);">readmore</a></div>
            </div>
            <div class="marketstat">
                <div class="marketstatcontent">
                    <div style="float: left; border-right:3px solid #009949; height:279px;">
                        <a href="http://www.dfm.ae" target="_blank">
                            <img src="images/dfmlogo.png" class="diagrm1" alt="Market Status 1" /></a>
                    </div>
                    <div style="float: left;">
                        <a href="http://www.adx.ae" target="_blank">
                            <img src="images/logo.png" class="diagrm1" alt="Market Status 2" style="margin-top:93px;" /></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
