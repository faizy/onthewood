﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using MawaridBLL;

namespace Mawarid
{
    public partial class OurTeamMembers : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            var masterPage = Master;
            if (masterPage == null) return;
            var lnkOurTeam = (HtmlAnchor)masterPage.FindControl("lnkOurTeam");
            lnkOurTeam.Attributes.Add("class", "active");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    InitializePage();
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void InitializePage()
        {
            var dRep = new OurTeamRepository();
            var dList = dRep.GetAllOurTeam();
            if (dList.Count > 0)
            {
                rptContent.DataSource = dList;
                rptContent.DataBind();
            }
        }

        protected void rptContent_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Edit")
                {
                    Response.Redirect("~/TeamMemberDetails.aspx?id=" + Utilities.Helper.Encrypt(e.CommandArgument.ToString()));
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}