﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using MawaridBLL;

namespace Mawarid
{
    public partial class BoardOfDirectors : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            var masterPage = Master;
            if (masterPage == null) return;
            var lnkBoardOfDirectors = (HtmlAnchor)masterPage.FindControl("lnkBoardOfDirectors");
            lnkBoardOfDirectors.Attributes.Add("class", "active");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    InitializePage();
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void InitializePage()
        {
            var dRep = new BoardOfDirectorsRepository();
            var dList = dRep.GetAllBoardOfDirector();
            if (dList.Count > 0)
            {
                rptContent.DataSource = dList;
                rptContent.DataBind();
            }
        }

        protected void rptContent_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Edit")
                {
                    Response.Redirect("~/BoardOfDirectorsDetails.aspx?id=" + Utilities.Helper.Encrypt(e.CommandArgument.ToString()));
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}