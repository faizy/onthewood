﻿<%@ Page Title="Mawarid | Contact Us" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="ContactUs.aspx.cs" Inherits="Mawarid.ContactUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <script type="text/javascript">
     $(document).ready(function () {
         $("#lnkContactUs").addClass("activeTop");
     });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div class="banner">
        <img src="images/aboutbanner.jpg" alt="mawarid" />
    </div>
    <div class="aboutcontent">
        <div class="maincont">
            <h2>
                CONTACT US</h2>
            <div class="clearfix">
            </div>
            <div class="continfocontainer">
                <div id="map">
                </div>
                <div class="contactinfo">
                    <p>
                        <span>Do you wish to join al Jazeera Team?</span> <span>Send your CV & Cover letter
                            to the following e-mail address</span><br />
                        <span id="lblEmail" runat="server" class="margincap"></span><span class="green">Tel (Main Office) :</span>
                        <span id="lblMobile" runat="server">+971 4 3041678</span><br />
                        <br />
                        <span class="green">Tel (Operation Department) :</span><br />
                        <span id="lblTel" runat="server"></span>
                        <br />
                        <br />
                        <span class="green">Working Hours :</span> <span id="lblWorkingHours" runat="server">
                            8:30 am - 4.30 pm from Sunday to Thursday.</span><br />
                        <br />
                    </p>
                </div>
            </div>
            <h2>
                ENQUIRY FORM</h2>
            <div class="clearfix">
                <div class="col1">
                    <label class="field1">
                        <asp:TextBox runat="server" ID="txtFName" placeholder="First Name"></asp:TextBox>
                        <br />
                    </label>
                    <label class="field1">
                        <asp:TextBox runat="server" ID="txtEmail" placeholder="Email"></asp:TextBox>
                        <br />
                    </label>
                </div>
                <div class="col1">
                    <label class="field2">
                        <asp:TextBox runat="server" ID="txtLName" placeholder="Last Name"></asp:TextBox>
                        <br />
                    </label>
                    <label class="field2">
                        <asp:TextBox runat="server" ID="txtPhone" placeholder="Phone"></asp:TextBox>
                        <br />
                    </label>
                </div>
                <div class="col1">
                    <label class="message">
                        <asp:TextBox runat="server" ID="txtMessage" placeholder="Message" TextMode="MultiLine"
                            Style="height: 60px; margin-top: 6px; resize: none; width: 255px;"></asp:TextBox>
                        <br />
                    </label>
                </div>
                <asp:Button runat="server" ID="btnSubmit" Text="SUBMIT" CssClass="formbtn" OnClick="btnSubmit_Click" />
            </div>
        </div>
        <div class="sidecontent">
            <div class="faq">
                <a href="~/FAQs.aspx" runat="server">
                    <h2>
                        F.A.Q</h2>
                    <ul class="faqlist">
                        <li>How do I open an account with Al Jazeera financial services ?</li>
                        <li>How do I give my orders to the Brokers at Al Jazeera ?</li>
                        <li>How do I know that my order has been filled either when buying shares or selling
                            them</li>
                        <li>How do I know that my order has been filled either when buying shares or selling
                            them</li>
                    </ul>
                </a>
            </div>
        </div>
    </div>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false" type="text/javascript"></script>
    <script type="text/javascript">
        var styles = [
  {
      "featureType": "landscape",
      "stylers": [
      { "saturation": 51 }
    ]
  }, {
      "featureType": "poi",
      "stylers": [
      { "saturation": -29 },
      { "lightness": 28 }
    ]
  }, {
      "featureType": "road",
      "stylers": [
      { "saturation": -58 }
    ]
  }, {
      "elementType": "labels.text.stroke",
      "stylers": [
      { "visibility": "on" }
    ]
  }, {
      "featureType": "water",
      "stylers": [
      { "saturation": 110 }
    ]
  }, {
      "featureType": "road",
      "elementType": "geometry.stroke",
      "stylers": [
      { "visibility": "off" }
    ]
  }
];

        var map;
        var mycenter = new google.maps.LatLng(25.155252, 55.228867);
        var myLatLang = new google.maps.LatLng(25.155252, 55.228867);

        function initialize() {
            var mapOptions = {
                zoom: 15,
                center: mycenter,
                scrollwheel: true,
                mapTypeControl: true,
                streetViewControl: false,
                panControl: true,
                draggable: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById('map'),
        mapOptions);
            var icon = new google.maps.MarkerImage("images/pin.png", null, null, null, new google.maps.Size(40, 40));
            var marker = new google.maps.Marker({
                position: myLatLang,
                map: map,
                title: "Mawarid",
                icon: icon
            });
            var styledMap = new google.maps.StyledMapType(styles, { name: "Styled Map" });

            map.mapTypes.set('map_style', styledMap);
            map.setMapTypeId('map_style');



            var mapOptions2 = {
                zoom: 16,
                center: mycenter2,
                scrollwheel: false,
                mapTypeControl: false,
                streetViewControl: false,
                panControl: true,
                draggable: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map2 = new google.maps.Map(document.getElementById(''),
        mapOptions2);
            var icon2 = new google.maps.MarkerImage("images/pin.png", null, null, null, new google.maps.Size(40, 40));
            var marker2 = new google.maps.Marker({
                position: myLatLang2,
                map: map2,
                title: "Mawarid",
                icon: icon2
            });

            map2.mapTypes.set('map_style', styledMap);
            map2.setMapTypeId('map_style');
        };
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
</asp:Content>
