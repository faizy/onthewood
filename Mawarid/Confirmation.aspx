﻿<%@ Page Title="Mawarid | Confirmation" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="Confirmation.aspx.cs" Inherits="Mawarid.Confirmation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div class="banner">
        <img src="images/aboutbanner.jpg" alt="mawarid" />
    </div>
    <div class="aboutcontent">
        <div class="maincont">
            <h2>
                CONFIRMATION</h2>
            <div class="clearfix">
            </div>
            <h3>
                Thankyou! Your Message has been received we will contact you soon.</h3>
        </div>
        <div class="sidecontent">
            <div class="faq">
                <a id="A1" href="~/FAQs.aspx" runat="server">
                    <h2>
                        F.A.Q</h2>
                    <ul class="faqlist">
                        <li>How do I open an account with Al Jazeera financial services ?</li>
                        <li>How do I give my orders to the Brokers at Al Jazeera ?</li>
                        <li>How do I know that my order has been filled either when buying shares or selling
                            them</li>
                        <li>How do I know that my order has been filled either when buying shares or selling
                            them</li>
                    </ul>
                </a>
            </div>
        </div>
    </div>
</asp:Content>
