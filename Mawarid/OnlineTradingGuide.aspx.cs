﻿using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using MawaridBLL;

namespace Mawarid
{
    public partial class OnlineTradingGuide : Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            var masterPage = Master;
            if (masterPage == null) return;
            var lnkOnlineTradingGuide = (HtmlAnchor)masterPage.FindControl("lnkOnlineTradingGuide");
            lnkOnlineTradingGuide.Attributes.Add("class", "active");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    InitializePage();
                }
            }
            catch (Exception ex)
            {
                    
            }
        }

        protected void InitializePage()
        {
            var dRep = new FilesRepository();
            var dList = dRep.GetAllFilesManagement();
            if (dList.Count > 0)
            {
                rptContent.DataSource = dList;
                rptContent.DataBind();
            }
        }
    }
}