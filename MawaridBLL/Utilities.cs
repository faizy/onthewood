﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Security.Cryptography;
using System.Web.UI.WebControls;


namespace MawaridBLL
{
    public class Utilities
    {
        //Generic functions
        public class Helper
        {
            public static void SetPleaseSelect(ref DropDownList ddl)
            {
                ddl.Items.Insert(0, new ListItem("Select", "0"));
            }
            public static string Encrypt(string toEncrypt)
            {
                var toEncryptArray = Encoding.UTF8.GetBytes(toEncrypt);
                var settingsReader = new AppSettingsReader();
                var key = (string)settingsReader.GetValue("SecurityKey", typeof(String));
                var hashmd5 = new MD5CryptoServiceProvider();
                var keyArray = hashmd5.ComputeHash(Encoding.UTF8.GetBytes(key));
                hashmd5.Clear();
                var tdes = new TripleDESCryptoServiceProvider
                {
                    Key = keyArray,
                    Mode = CipherMode.ECB,
                    Padding = PaddingMode.PKCS7
                };
                var cTransform = tdes.CreateEncryptor();
                var resultArray =
                  cTransform.TransformFinalBlock(toEncryptArray, 0,
                  toEncryptArray.Length);
                tdes.Clear();
                var returnString = Convert.ToBase64String(resultArray, 0, resultArray.Length);
                if (returnString.Contains("+"))
                {
                    returnString = returnString.Replace("+", "$");
                }
                return returnString;
            }
            public static string Decrypt(string cipherString)
            {
                if (cipherString.Contains("$"))
                {
                    cipherString = cipherString.Replace("$", "+");
                }
                var toEncryptArray = Convert.FromBase64String(cipherString);
                var settingsReader = new AppSettingsReader();
                var key = (string)settingsReader.GetValue("SecurityKey", typeof(String));
                var hashmd5 = new MD5CryptoServiceProvider();
                var keyArray = hashmd5.ComputeHash(Encoding.UTF8.GetBytes(key));
                hashmd5.Clear();
                var tdes = new TripleDESCryptoServiceProvider
                {
                    Key = keyArray,
                    Mode = CipherMode.ECB,
                    Padding = PaddingMode.PKCS7
                };
                var cTransform = tdes.CreateDecryptor();
                var resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
                tdes.Clear();
                return Encoding.UTF8.GetString(resultArray);
            }            
            public static string IsNull(string txt)
            {
                if (string.IsNullOrEmpty(txt))
                {
                    return "N/A";
                }
                else
                {
                    return txt;
                }
            }
        }

        //Record Status
        public class RecordStatus
        {
            public const string Active = "Active";
            public const string InActive = "In Active";
            public const string Deleted = "Deleted";
        }
    }
}
