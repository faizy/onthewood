﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace MawaridBLL
{
    public class BoardOfDirectorsRepository
    {
        public int InsertBoardOfDirector(BoardOfDirector boardOfDirector)
        {
            boardOfDirector.AddedDate = DateTime.Now.Date;
            boardOfDirector.ModifiedDate = DateTime.Now.Date;
            int scopeId;
            using (var conn = new MawaridEntities())
            {
                conn.AddToBoardOfDirectors(boardOfDirector);
                conn.SaveChanges();
                scopeId = boardOfDirector.BoardOfDirectorId;
            }
            return scopeId;
        }

        public int UpdateBoardOfDirector(BoardOfDirector boardOfDirector)
        {
            const string status = Utilities.RecordStatus.Deleted;
            int scopeId;
            using (var conn = new MawaridEntities())
            {
                var entity = conn.BoardOfDirectors.FirstOrDefault(x => x.RecordStatus != status && x.BoardOfDirectorId == boardOfDirector.BoardOfDirectorId);
                if (entity != null)
                {
                    entity.Name = boardOfDirector.Name;
                    entity.Designation = boardOfDirector.Designation;
                    entity.Details = boardOfDirector.Details;
                    entity.Image = boardOfDirector.Image;
                    entity.ModifiedDate = boardOfDirector.ModifiedDate;
                    entity.RecordStatus = boardOfDirector.RecordStatus;
                    conn.ObjectStateManager.ChangeObjectState(entity, EntityState.Modified);
                }
                conn.SaveChanges();
                scopeId = boardOfDirector.BoardOfDirectorId;
            }
            return scopeId;
        }

        public List<BoardOfDirector> GetAllBoardOfDirector()
        {
            List<BoardOfDirector> entity;
            const string status = Utilities.RecordStatus.Deleted;
            using (var conn = new MawaridEntities())
            {
                conn.BoardOfDirectors.MergeOption = System.Data.Objects.MergeOption.NoTracking;
                entity = conn.BoardOfDirectors.Where(x => x.RecordStatus != status).OrderByDescending(y => y.ModifiedDate).ToList();
            }
            return entity;
        }

        public BoardOfDirector GetBoardOfDirectorById(int boardOfDirectorId)
        {
            BoardOfDirector entity;
            const string status = Utilities.RecordStatus.Deleted;
            using (var conn = new MawaridEntities())
            {
                conn.BoardOfDirectors.MergeOption = System.Data.Objects.MergeOption.NoTracking;
                entity = conn.BoardOfDirectors.FirstOrDefault(x => x.RecordStatus != status && x.BoardOfDirectorId == boardOfDirectorId);
            }
            return entity;
        }

        public void DeleteBoardOfDirector(int boardOfDirectorId)
        {
            const string status = Utilities.RecordStatus.Deleted;
            using (var conn = new MawaridEntities())
            {
                var entity = conn.BoardOfDirectors.FirstOrDefault(x => x.BoardOfDirectorId == boardOfDirectorId && x.RecordStatus != status);
                if (entity != null)
                {
                    entity.RecordStatus = status;
                    conn.ObjectStateManager.ChangeObjectState(entity, EntityState.Modified);
                }
                conn.SaveChanges();
            }
        }
    }
}
