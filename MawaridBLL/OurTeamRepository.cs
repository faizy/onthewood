﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace MawaridBLL
{
    public class OurTeamRepository
    {
        public int InsertOurTeam(OurTeam ourTeam)
        {
            int scopeId;
            ourTeam.AddedDate = DateTime.Now.Date;
            ourTeam.ModifiedDate = DateTime.Now.Date;
            using (var conn = new MawaridEntities())
            {
                conn.AddToOurTeams(ourTeam);
                conn.SaveChanges();
                scopeId = ourTeam.OurTeamId;
            }
            return scopeId;
        }

        public int UpdateOurTeam(OurTeam ourTeam)
        {
            const string status = Utilities.RecordStatus.Deleted;
            int scopeId;
            using (var conn = new MawaridEntities())
            {
                var entity = conn.OurTeams.FirstOrDefault(x => x.RecordStatus != status && x.OurTeamId == ourTeam.OurTeamId);
                if (entity != null)
                {
                    entity.Name = ourTeam.Name;
                    entity.Designation = ourTeam.Designation;
                    entity.Details = ourTeam.Details;
                    entity.Image = ourTeam.Image;                    
                    entity.RecordStatus = ourTeam.RecordStatus;
                    entity.ModifiedDate = ourTeam.ModifiedDate;
                    conn.ObjectStateManager.ChangeObjectState(entity, EntityState.Modified);
                }
                conn.SaveChanges();
                scopeId = ourTeam.OurTeamId;
            }
            return scopeId;
        }

        public List<OurTeam> GetAllOurTeam()
        {
            List<OurTeam> entity;
            const string status = Utilities.RecordStatus.Deleted;
            using (var conn = new MawaridEntities())
            {
                conn.OurTeams.MergeOption = System.Data.Objects.MergeOption.NoTracking;
                entity = conn.OurTeams.Where(x => x.RecordStatus != status).OrderByDescending(y => y.ModifiedDate).ToList();
            }
            return entity;
        }

        public OurTeam GetOurTeamById(int ourTeamId)
        {
            OurTeam entity;
            const string status = Utilities.RecordStatus.Deleted;
            using (var conn = new MawaridEntities())
            {
                conn.OurTeams.MergeOption = System.Data.Objects.MergeOption.NoTracking;
                entity = conn.OurTeams.FirstOrDefault(x => x.RecordStatus != status && x.OurTeamId == ourTeamId);
            }
            return entity;
        }

        public void DeleteOurTeam(int ourTeamId)
        {
            const string status = Utilities.RecordStatus.Deleted;
            using (var conn = new MawaridEntities())
            {
                var entity = conn.OurTeams.FirstOrDefault(x => x.OurTeamId == ourTeamId && x.RecordStatus != status);
                if (entity != null)
                {
                    entity.RecordStatus = status;
                    conn.ObjectStateManager.ChangeObjectState(entity, EntityState.Modified);
                }
                conn.SaveChanges();
            }
        }
    }
}
