﻿using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace MawaridBLL
{
    public class ContactUsRepository
    {
        public void UpdateContactUs(ContactU entity)
        {

            using (var conn = new MawaridEntities())
            {
                var contact = conn.ContactUs.FirstOrDefault(x => x.Id == 1);
                if (contact != null)
                {
                    contact.Email = entity.Email;
                    contact.Tel = entity.Tel;
                    contact.Mobile = entity.Mobile;
                    contact.Working_Hours = entity.Working_Hours;                    
                }
                conn.SaveChanges();
            }
        }

        public ContactU GetContactUsById()
        {
            ContactU entity;
            using (var conn = new MawaridEntities())
            {
                conn.FilesManagements.MergeOption = System.Data.Objects.MergeOption.NoTracking;
                entity = conn.ContactUs.FirstOrDefault(x => x.Id == 1);
            }
            return entity;
        }
    }
}
