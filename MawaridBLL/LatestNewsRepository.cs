﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace MawaridBLL
{
    public class LatestNewsRepository
    {
        public int InsertLatestNews(LatestNew latestNews)
        {
            int scopeId;
            latestNews.AddedDate = DateTime.Now.Date;
            latestNews.ModifiedDate = DateTime.Now.Date;
            using (var conn = new MawaridEntities())
            {
                conn.AddToLatestNews(latestNews);
                conn.SaveChanges();
                scopeId = latestNews.NewsId;
            }
            return scopeId;
        }

        public int UpdateLatestNews(LatestNew latestNews)
        {
            const string status = Utilities.RecordStatus.Deleted;
            int scopeId;
            using (var conn = new MawaridEntities())
            {
                var entity = conn.LatestNews.FirstOrDefault(x => x.RecordStatus != status && x.NewsId == latestNews.NewsId);
                if (entity != null)
                {
                    entity.Description = latestNews.Description;
                    entity.Image = latestNews.Image;
                    entity.ModifiedDate = latestNews.ModifiedDate;
                    entity.Title = latestNews.Title;
                    entity.RecordStatus = latestNews.RecordStatus;
                    conn.ObjectStateManager.ChangeObjectState(entity, EntityState.Modified);
                }
                conn.SaveChanges();
                scopeId = latestNews.NewsId;
            }
            return scopeId;
        }

        public List<LatestNew> GetAllLatestNews()
        {
            List<LatestNew> entity;
            const string status = Utilities.RecordStatus.Deleted;
            using (var conn = new MawaridEntities())
            {
                conn.LatestNews.MergeOption = System.Data.Objects.MergeOption.NoTracking;
                entity = conn.LatestNews.Where(x => x.RecordStatus != status).OrderByDescending(y => y.ModifiedDate).ToList();
            }
            return entity;
        }

        public LatestNew GetLatestNewsById(int newsId)
        {
            LatestNew entity;
            const string status = Utilities.RecordStatus.Deleted;
            using (var conn = new MawaridEntities())
            {
                conn.LatestNews.MergeOption = System.Data.Objects.MergeOption.NoTracking;
                entity = conn.LatestNews.FirstOrDefault(x => x.RecordStatus != status && x.NewsId == newsId);
            }
            return entity;
        }

        public void DeleteLatestNews(int newsId)
        {
            const string status = Utilities.RecordStatus.Deleted;
            using (var conn = new MawaridEntities())
            {
                var entity = conn.LatestNews.FirstOrDefault(x => x.NewsId == newsId && x.RecordStatus != status);
                if (entity != null)
                {
                    entity.RecordStatus = status;
                    conn.ObjectStateManager.ChangeObjectState(entity, EntityState.Modified);
                }
                conn.SaveChanges();
            }
        }
    }
}
