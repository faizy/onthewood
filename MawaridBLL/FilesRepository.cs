﻿using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace MawaridBLL
{
    public class FilesRepository
    {        
        public int InsertFilesManagement(FilesManagement filesManagement)
        {
            int scopeId;            
            using (var conn = new MawaridEntities())
            {
                conn.AddToFilesManagements(filesManagement);
                conn.SaveChanges();
                scopeId = filesManagement.FileManagementId;
            }
            return scopeId;
        }

        public List<FilesManagement> GetAllFilesManagement()
        {
            List<FilesManagement> entity;            
            using (var conn = new MawaridEntities())
            {
                conn.FilesManagements.MergeOption = System.Data.Objects.MergeOption.NoTracking;
                entity = conn.FilesManagements.OrderByDescending(y => y.AddedDate).ToList();
            }
            return entity;
        }
        public int UpdateFilesManagement(FilesManagement boardOfDirector)
        {
            //const string status = Utilities.RecordStatus.Deleted;
            int scopeId;
            using (var conn = new MawaridEntities())
            {
                var entity = conn.FilesManagements.FirstOrDefault(x => x.FileManagementId == boardOfDirector.FileManagementId);
                if (entity != null)
                {
                    entity.Name = boardOfDirector.Name;
                    entity.AddedDate = boardOfDirector.AddedDate;
                    conn.ObjectStateManager.ChangeObjectState(entity, EntityState.Modified);
                }
                conn.SaveChanges();
                scopeId = boardOfDirector.FileManagementId;
            }
            return scopeId;
        }

        public FilesManagement GetFilesManagementById(int filesManagementId)
        {
            FilesManagement entity;            
            using (var conn = new MawaridEntities())
            {
                conn.FilesManagements.MergeOption = System.Data.Objects.MergeOption.NoTracking;
                entity = conn.FilesManagements.FirstOrDefault(x => x.FileManagementId == filesManagementId);
            }
            return entity;
        }
        
        public void DeleteFilesManagement(int filesManagementId)
        {            
            using (var conn = new MawaridEntities())
            {
                var entity = conn.FilesManagements.FirstOrDefault(x => x.FileManagementId == filesManagementId);
                if (entity != null)
                {                    
                    conn.FilesManagements.DeleteObject(entity);
                    conn.ObjectStateManager.ChangeObjectState(entity, EntityState.Deleted);
                }
                conn.SaveChanges();
            }
        }
    }
}
